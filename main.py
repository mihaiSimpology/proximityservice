import json

import boto3
import botocore
import logging

S3 = boto3.resource("s3")
logger = logging.getLogger()
logger.setLevel(logging.INFO)

def make_log(event):
    logger.info(event)

def download_data(bucket, key):
    logger.info("download_data function has been called with the following parameters:" + bucket + " / " + key)
    path = "/tmp/" + key
    res = S3.Bucket(bucket).download_file(key, path)
    return res

def put_s3_data(data, bucket, key):
    logger.info("put_s3_data function called with bucket and key:" + bucket + "/" + key)
    obj = S3.Object(bucket, key)
    return obj.put(Body=data)

def get_s3_data(bucket: str, key: str):
    logger.info("get_s3_data function has been called with the following parameters:" + bucket + " / " + key)
    obj = S3.Object(bucket, key)
    return obj.get()['Body'].read().decode('utf-8')

def http_response(statusCode, body):
    
    logger.info("Response sent from lambda with status code:" + str(statusCode))
    event = {"statusCode":statusCode, "isBase64Encoded" : False, "body": body, "headers": {}}

    return event

def common_member(a, b): 
    a_set = set(a) 
    b_set = set(b) 
    if (a_set & b_set): 
        return True 
    else: 
        return False


def midpoint(p1, p2):

    return  ((p1["x"] + p2["x"])/2,(p1["y"] + p2["y"])/2)

def get_data_coordinates(data):

    space = data["fullTextAnnotation"]["pages"][0]["blocks"]

    words_space = []
    paragraphs_space = []

    for block in space:
        for p in block["paragraphs"]:
            words = []
            for word in p["words"]:
                metadata = {"coordinates":word["boundingBox"]["vertices"]}
                letters = [s["text"] for s in word["symbols"]]
                metadata["word"] = "".join(letters)
                words.append(metadata)
                if "detectedBreak" in word["symbols"][-1]["property"]:
                    if word["symbols"][-1]["property"]["detectedBreak"]["type"] in ["LINE_BREAK","EOL_SURE_SPACE"]:
                        paragraphs_space.append(words)
                        words_space += words
                        words = []

    return words_space, paragraphs_space

def check_intersection(p1, p2, s1, s2, epsilon):

    if s1[0] < s2[0]:
        dom_x1 , dom_x2 = s1[0],s2[0]
    else:
        dom_x1 , dom_x2 = s2[0],s1[0]

    if s1[1] < s2[1]:
        cod_y1 , cod_y2 = s1[1],s2[1]
    else:
        cod_y1 , cod_y2 = s2[1],s1[1]
    
    if (p1[0] - p2[0]) == 0:
        if (dom_x1 < p1[0] and p1[0] < dom_x2):
            return True
        else:
            return False 
    else:
        a = (p1[1] - p2[1])/(p1[0] - p2[0])
        b = (p1[0]*p2[1] - p1[1]*p2[0])/(p1[0] - p2[0])

    dom = [i for i in range(dom_x1, dom_x2+1, epsilon)]

    for x in dom:
        if cod_y1 < a*x + b and a*x + b < cod_y2:
            return True

    return False

    
def find_coordinate(words_space,paragraphs_space, to_search, direction):

    epsilon = 1
    tolerance = 5
    found_coordinates = []

    to_search = to_search.split()

    for w in words_space:
        if w["word"] in to_search:
            found_coordinates.append(w)
    
    for found in found_coordinates:
        words_space.remove(found)

    first = found_coordinates[0]
    last = found_coordinates[-1]

    upper_left_corner = first["coordinates"][0]
    upper_right_corner = last["coordinates"][1]

    lower_right_corner = last["coordinates"][2]
    lower_left_corner = first["coordinates"][3]
    
    horizontal_point_1 = midpoint(upper_left_corner, lower_left_corner)
    horizontal_point_2 = midpoint(upper_right_corner, lower_right_corner)

    vertical_point_1 = midpoint(upper_left_corner, upper_right_corner)
    vertical_point_2 = midpoint(lower_left_corner, lower_right_corner)

    found = []
    
    for p in paragraphs_space:

        first = p[0]
        last = p[-1]

        c = {}
        c["coordinates"] = {} 
        c["coordinates"][0] = first["coordinates"][0]
        c["coordinates"][3] = first["coordinates"][3]
        c["coordinates"][1] = last["coordinates"][1]
        c["coordinates"][2] = last["coordinates"][2]
       

        v_1 = c["coordinates"][1]["x"], c["coordinates"][1]["y"]
        v_2 = c["coordinates"][2]["x"], c["coordinates"][2]["y"]

        h_1 = c["coordinates"][0]["x"], c["coordinates"][0]["y"]
        h_2 = c["coordinates"][1]["x"], c["coordinates"][1]["y"]

        if direction == "UP":
            if check_intersection(vertical_point_1, vertical_point_2, h_1, h_2, epsilon) and h_1[1] <= upper_left_corner["y"] + tolerance:
                found.append(p)
                continue
        if direction == "LEFT":
            if check_intersection(horizontal_point_1, horizontal_point_2, v_1, v_2, epsilon) and v_1[0] - tolerance <= upper_left_corner["x"]:
                found.append(p)
                continue
        if direction == "DOWN":
            if check_intersection(vertical_point_1, vertical_point_2, h_1, h_2, epsilon) and h_1[1] + tolerance >= lower_left_corner["y"]:
                found.append(p)
                continue
        if direction == "RIGHT":
            if check_intersection(horizontal_point_1, horizontal_point_2, v_1, v_2, epsilon) and v_1[0] - tolerance <= upper_right_corner["x"] :
                found.append(p)
                continue

    return found

def handler(event, context):

    make_log(event)    
    event = json.loads(event['Messages'][0]["Body"])
    #event = event['Messages'][0]["Body"]
    make_log(event)

    bucket_rules = event["bucket_rule"]
    key_rules = event["key_rule"]

    bucket_sample = event["bucket_sample"]
    key_sample = event["key_sample"]

    rule = json.loads(get_s3_data(bucket_rules, key_rules))

    to_search = rule["to_search"]
    direction = rule["direction"]
    order = rule["order"]
    words = rule["words"]
    in_chars = rule["in_chars"]
    
    between = list(map(lambda x:int(x),rule["between"].split("-")))

    data = json.loads(get_s3_data(bucket_sample, key_sample))

    coordinates, paragraphs_space = get_data_coordinates(data)

    found = find_coordinate(coordinates,paragraphs_space, to_search, direction)

    strings = [" ".join([word["word"] for word in p]) for p in found]

    if order != None:
        strings = strings[order]

    if words != None:
        for c in words:
            strings= list(filter(lambda x: c in x, strings))
    
    if between != None:
        strings = strings[between[0]:between[1]]

    if in_chars != None:
        new_strings = []
        for s in strings:
            words = s.split(" ")
            words = list(filter(lambda x: common_member(in_chars, list(x)),words))
            new_strings.append(" ".join(words))
        strings = new_strings

            
    val = strings
    return http_response(200,body=val)