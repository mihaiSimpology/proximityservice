import json
from main import handler


def main(event):
    return handler(event, None)

if __name__ == "__main__":
    s = '{"Messages":[{"Body":{"bucket_sample":"google-ocr-destination","key_sample":"test_image.json", "bucket_rule":"bucket-rules","key_rule":"proximity-rules.json"}}]}'
    event = json.loads(s)
    response = main(event)

    print(response)