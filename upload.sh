zip proximity.zip main.py 
aws s3 cp proximity.zip s3://ml-dev.simpology2
aws lambda update-function-code --function-name "ProximityService" --region "eu-central-1" --s3-bucket "ml-dev.simpology2" --s3-key "proximity.zip"
aws lambda invoke --function-name "ProximityService"  --payload '{"Messages":[{"Body":"{\"bucket_sample\":\"google-ocr-destination\",\"key_sample\":\"test_image.json\", \"bucket_rule\":\"bucket-rules\",\"key_rule\":\"proximity-rules.json\"}"}]}' here --log-type Tail --query 'LogResult' --output text |  base64 -d
rm proximity.zip
cat here
